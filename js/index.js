import {
    AppLaptop
} from './laptop.js'

class App {
    constructor() {

        this.elStatus = document.getElementById('app-status')
        this.laptop = new AppLaptop()

        //DOM-elements
        this.workBtn = document.getElementById('workBtn')
        this.bankBtn = document.getElementById('bankBtn')
        this.getLoanBtn = document.getElementById('loanBtn')
        this.buyBtn = document.getElementById('buyBtn')
        this.repayLoanBtn = document.getElementById('repayLoanBtn')

        //Properties
        this.bankBalance = 224500
        this.payBalance = 0
        this.hasOutstandingLoan = false
        this.outstandingLoanAmount = 0
        this.payRate = 100
        this.personName = "Jane Banker"
        this.ownedLaptops = []
        this.numberOfTotalLoans = 0

    }

    async init() {
        await this.laptop.init()
        this.render()
        this.startEventListener()
        this.displayBalances()
        this.displayPersonDetails()
        this.checkOutstandingLoan()
        this.displayOwnedLaptops()

    }


    //event listeners
    startEventListener() {
        this.workBtn.addEventListener('click', this.work.bind(this))
        this.bankBtn.addEventListener('click', this.updateBankBalance.bind(this))
        this.getLoanBtn.addEventListener('click', this.getLoan.bind(this))
        this.buyBtn.addEventListener('click', this.buyLaptop.bind(this))
        this.repayLoanBtn.addEventListener('click', this.repayLoan.bind(this))

    }

    render() {
        this.laptop.render()
    }

    //increases the payBalance
    work() {
        this.payBalance += this.payRate
        this.displayBalances()
    }

    //displays the current balances
    displayBalances() {
        document.getElementById("bankBalance").innerHTML = this.bankBalance + " kr"
        document.getElementById("payBalance").innerHTML = this.payBalance + " kr"
        this.checkOutstandingLoan()
        this.displayLoanBalance()
    }

    //checks if user has a loan.
    hasLoan() {
        if (this.outstandingLoanAmount > 0) {
            return true
        } else {
            return false
        }
    }

    //repays the loan
    repayLoan() {
        if (this.payBalance < this.outstandingLoanAmount) {
            this.outstandingLoanAmount = this.outstandingLoanAmount - this.payBalance
            this.payBalance = 0
        } else {
            let toBank = this.payBalance - this.outstandingLoanAmount
            this.bankBalance += toBank
            this.payBalance = 0
            this.outstandingLoanAmount = 0
        }

        this.displayBalances()
    }


    displayPersonDetails() {
        document.getElementById("personName").innerHTML = this.personName
    }

    //hides/shows the repay loan btn
    checkOutstandingLoan() {
        let elRepayLoanBtn = document.getElementById("repayLoanBtn")
        if (this.hasLoan()) {
            elRepayLoanBtn.style.display = "block"
        } else {
            elRepayLoanBtn.style.display = "none"
        }
        console.log(this.hasLoan())
    }

    //transfers the payBalance to the bank balance
    updateBankBalance() {
        if (this.hasLoan()) {
            let deductionAmount = (10 / 100) * this.payBalance
            this.payBalance = this.payBalance - deductionAmount
            this.outstandingLoanAmount = this.outstandingLoanAmount - deductionAmount
            this.bankBalance += this.payBalance
            this.payBalance = 0

        } else {
            this.bankBalance += this.payBalance
            this.payBalance = 0
        }
        this.displayBalances()
    }


    //attemps to get a loan
    getLoan() {
        this.checkLoanConstraints(this.getLoanAmount())
        this.checkOutstandingLoan()
    }

    //gets the loan amount from the user with a prompt
    getLoanAmount() {
        let desiredAmount = prompt("Please enter a loan amount")
        if (desiredAmount == null || desiredAmount == "") {
            console.log("User cancelled")
        } else {
            return desiredAmount
        }
    }


    //checks the loan constraints
    checkLoanConstraints(desiredLoanAmount) {
        if (desiredLoanAmount > 2 * this.bankBalance) {
            alert("Desired loan amount too big. The maximum loan amount you can get is " + 2 * this.bankBalance + " kr")
        }
        if (this.hasLoan()) {
            alert("You already have a loan! You need to pay it back before getting another loan")
        }
        if (this.numberOfTotalLoans != 0 && this.ownedLaptops.length === 0) {
            alert("You cannot have more than one bank loan before buying a computer")
        } else if (desiredLoanAmount < 2 * this.bankBalance && !this.hasLoan()) {
            this.numberOfTotalLoans++
            this.updateLoanBalance(desiredLoanAmount)
            this.displayBalances()
        }
    }

    //updates the loan balance
    updateLoanBalance(desiredLoanAmount) {
        this.outstandingLoanAmount = desiredLoanAmount
        this.displayBalances()
    }


    //hides/shows the loan balance
    displayLoanBalance() {
        let title = document.getElementById("loanTitle")
        let balance = document.getElementById("loanBalance")
        if (this.hasLoan()) {
            title.style.display = "block"
            balance.style.display = "block"
            title.innerHTML = "Loan:"
            balance.innerHTML = this.outstandingLoanAmount + " kr"
        } else {
            title.style.display = "none"
            balance.style.display = "none"
        }
        this.displayLoanDetails();
    }

    //displays the loan details text
    displayLoanDetails() {
        let loanDetails = document.getElementById("loanDetails")
        if (this.hasLoan()) {
            loanDetails.style.display = "block"
            loanDetails.innerHTML = "You have a loan of " + this.outstandingLoanAmount + " kr. Please pay it back as soon as possible"
        } else {
            loanDetails.style.display = "none"
        }
    }

    //lists the owned laptops
    displayOwnedLaptops() {
        let elOwnedLaptops = document.getElementById("ownedLaptops")
        let elLaptopList = document.getElementById("laptopList")

        if (this.ownedLaptops.length === 0) {
            elOwnedLaptops.innerHTML = "You don't own any laptops :("
        } else {
            elOwnedLaptops.innerHTML = "<b>Owned laptops: </b>"
            const laptops = this.ownedLaptops.map(name => `<li>${name}</li>`).join('')
            elLaptopList.innerHTML = laptops
        }
    }
    //attempts to buy a laptop. If no laptop is selected the user will be alerted to select a laptop first
    //if the user has enough money to buy a laptop the laptop will be added to the ownedLaptops variable
    buyLaptop() {
        if (this.laptop.selectedLaptop === null) {
            alert("Please select a laptop first!!")
        } else {
            let laptopPrice = this.laptop.selectedLaptop.price

            if (laptopPrice > this.bankBalance) {
                alert(`You can not buy this computer. You need to work more! (Or get a loan)`)
            } else {
                this.bankBalance = this.bankBalance - laptopPrice
                this.displayBalances()
                this.ownedLaptops.push(this.laptop.selectedLaptop.name)
                alert("Congratulations, you now own the " + this.laptop.selectedLaptop.name + " laptop!")
            }
            this.displayOwnedLaptops()

        }

    }

}

new App().init()
import { fetchLaptops } from "./api.js";
import { AppLaptopInformation } from './laptop-info.js'


export class AppLaptop {

    // DOM-elements
    elLaptopSelect = document.getElementById('selectLaptop')
    elOnsalebadge = document.getElementById( 'saleBadge')
  
    // Data properties
    laptops = []
    selectedLaptop = null

    // Computer components
    selectedLaptopInformation = new AppLaptopInformation()

    // Status properties
    error = ''

    async init() {
        this.createEventListeners()
        await this.createLaptops()        
    }

    async createLaptops() {
        try {
            this.elLaptopSelect.disabled = true
            this.laptops = await fetchLaptops()
                .then(laptops =>
                    laptops.map(laptop => new Laptop(laptop))
                )
        } catch (e) {
            this.error = e.message
            console.log(e);
        }
    }

    createEventListeners() {
        // Event Listeners
        this.elLaptopSelect.addEventListener('change', this.onLaptopChange.bind(this))
        
    }

    onLaptopChange() {

        if (parseInt(this.elLaptopSelect.value) === -1) {
            // Reset the current computer.
            return;
        }

        this.selectedLaptop = this.laptops.find(laptop => {
            return laptop.id == this.elLaptopSelect.value
        })

        this.selectedLaptopInformation.setLaptop( this.selectedLaptop )
    }

    createDefaultOptionForSelect() {
        const elLaptop = document.createElement('option')
        elLaptop.innerText = '-- Select a laptop --'
        elLaptop.value = -1
        this.elLaptopSelect.appendChild(elLaptop)
    }

    render() {
       
        this.createDefaultOptionForSelect()

        this.laptops.forEach(laptop => {
            this.elLaptopSelect.appendChild(laptop.createLaptopSelectOption())
        })

        this.elLaptopSelect.disabled = false

        this.selectedLaptopInformation.render()
    }
}

export class Laptop {
    constructor(laptop) {
        this.id = laptop.id
        this.name = laptop.name
        this.description = laptop.description
        this.img = laptop.img
        this.onSale = laptop.onSale
        this.features = laptop.features
        this.price = laptop.price
    }

    createLaptopSelectOption() {
        const elLaptopOption = document.createElement('option')
        elLaptopOption.value = this.id
        elLaptopOption.innerText = this.name
        return elLaptopOption
    }
}

export class AppLaptopInformation {

    elLaptopInformation = document.getElementById('selected-laptop-information')
    elLaptopDescription = document.getElementById('laptopDescription')
    elStoreFeatureListDisplay = document.getElementById('featureList')
    elLaptopCard = document.getElementById('showLaptopDetails')
    elOnsalebadge = document.getElementById('saleBadge')
    laptop;

    setLaptop(laptop) {
        this.laptop = laptop
        this.render()
    }

    render() {

        if (!this.laptop) {
            return;
        }else{
            
        }

        // Clear the current laptop
        this.elLaptopInformation.innerHTML = ''
        

        // Create a new container for the laptop-image to be rendered in.
        const elLaptop = document.createElement( 'div' )

        const elLaptopPrice = document.getElementById('laptopPrice')
        elLaptopPrice.innerText = `${this.laptop.price}  NOK`

        const elLaptopName = document.getElementById( 'laptopName' )
        elLaptopName.innerText = this.laptop.name

        const elLaptopDescription = document.getElementById( 'laptopDescription' );
        elLaptopDescription.innerText = this.laptop.description

        const elLaptopFeatures = document.getElementById( 'laptopFeatures' );
        const featureList = this.laptop.features.map(feature => `<li>${feature}</li>`).join('');
        elLaptopFeatures.innerHTML = featureList;

        const elLaptopImageContainer = document.createElement('div')
        const image = new Image()
        image.width = 320
        image.onload = () => {
            elLaptopImageContainer.appendChild( image )
        }

        image.src = this.laptop.img;
        console.log( this.laptop.img )
        image.id="laptopImage"
        elLaptop.appendChild( elLaptopImageContainer )

        //checks if laptop is on sale and displays the "on sale"-badge
        if(this.laptop.onSale === true){
            this.elOnsalebadge.style.visibility = "visible"
        }else{
            this.elOnsalebadge.style.visibility = "hidden"
        }

        this.elLaptopInformation.appendChild( elLaptop )
    }
}

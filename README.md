# Komputer Store

The first javascript assignment for the Experis Academy .NET upskill course winter 2021. The computer store allows users to earn money by "working" and buying computers listed in the store. 

## Features
The user can work buy clicking on the "Work"-button. The user earns 100kr each time they click the button. The user can transfer their earned money to the bank by clicking the "Bank"-button. The bank balance can be spent on buying computers from the store. 
The user can also apply for a loan by clicking the "Get a loan"-button. The bank only allows users to have one loan at the time. Loans can be repaid with the "Repay loan"-button. The users can also select different laptops from the dropdown menu in the "Laptops"-section. The name, features, description and price of the laptop is listed and the user can buy the selected laptop bu clicking the "Buy now"-button. The user is allowed to purchase the selected laptop if the bank balance is greater than the price of the laptop. The names of the purcased laptops will be listed in the "Owned laptops"-section.

## Prerequisites
<ul>
<li>Visual Studio Code</li>
<li>Live server extension for Visual Studio Code</li>
<li>Node.js</li>
</ul>

## Setup
<ul>
<li>Clone the project</li>
<li>npm install</li>
<li>npm start</li>
<li>Open the index.html-file with Live Server</li>
</ul>